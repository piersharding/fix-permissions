/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
// +kubebuilder:docs-gen:collapse=Apache License

// Go imports
package v1

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"

	// Import all Kubernetes client auth plugins (e.g. Azure, GCP, OIDC, etc.)
	// to ensure that exec-entrypoint and run can make use of them.

	_ "k8s.io/client-go/plugin/pkg/client/auth"
	"sigs.k8s.io/controller-runtime/pkg/client"

	//+kubebuilder:scaffold:imports

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
)

var (
	codecs       = serializer.NewCodecFactory(runtime.NewScheme())
	deserializer = codecs.UniversalDeserializer()
)

type PodAnnotator struct {
	Client  client.Client
	decoder *admission.Decoder
}

// +kubebuilder:webhook:path=/mutate-v1-pod,mutating=true,failurePolicy=ignore,groups="",resources=pods;deployments,verbs=create;update;delete,versions=v1,admissionReviewVersions=v1,sideEffects=None,name=mpod.skao.int

// https://github.com/trstringer/kubernetes-mutating-webhook/blob/main/cmd/root.go
func (a *PodAnnotator) Handle(ctx context.Context, req admission.Request) admission.Response {
	log := logf.FromContext(ctx)

	// log.Info("Annotated Pod request: " + fmt.Sprintf("%v", req))

	// Decode the Pod from the AdmissionReview.
	pod := corev1.Pod{}
	if _, _, err := deserializer.Decode(req.Object.Raw, nil, &pod); err != nil {
		log.Info("SOME OTHER OBJECT")
		return admission.Errored(http.StatusBadRequest, err)
	}

	// mutate the fields in Pod
	if pod.Annotations == nil {
		pod.Annotations = map[string]string{}
	}
	log.Info("Annotated Pod: " + fmt.Sprintf("operation: %s %s/%s", req.Operation, pod.Namespace, pod.Name))
	if req.Operation == "DELETE" {
		log.Info("Pod: (RETURNING) " + fmt.Sprintf("operation: %s %s/%s", req.Operation, pod.Namespace, pod.Name))
		return admission.Allowed("Delete Pod allowed")
	}

	if _, ok := pod.Annotations["skip-fix-permissions"]; ok {
		log.Info("Pod: (RETURNING) " + fmt.Sprintf("annotations: %v %s/%s", pod.Annotations, pod.Namespace, pod.Name))
		return admission.Allowed("Skip Pod fix")
	}

	// volumes:
	// - name: data
	//   persistentVolumeClaim:
	// 	claimName: ska-tango-base-tangodb-storage
	// cp -f /usr/bin/true /usr/bin/chown
	// lifecycle:
	// 	postStart:
	// 	exec:
	// 		command:
	// 		- /bin/sh
	// 		- -c
	// 		- cp -f /usr/bin/true /usr/bin/chown; exit 0
	var hasClaim bool = false
	for _, v := range pod.Spec.Volumes {
		if v.PersistentVolumeClaim != nil {
			if v.PersistentVolumeClaim.ClaimName != "" {
				hasClaim = true
			}
		}
	}
	if !hasClaim {
		log.Info("Pod: (RETURNING NO CLAIM) " + fmt.Sprintf("annotations: %v %s/%s", pod.Annotations, pod.Namespace, pod.Name))
		return admission.Allowed("Pod allowed")
	}

	log.Info("Incoming Pod: volumes are - " + fmt.Sprintf("%v", pod.Spec.Volumes))
	var hasTango bool = false

	label_val, ok := pod.ObjectMeta.Labels["cluster-name"]
	// If the label exists
	if ok {
		if label_val == "timescaledb" {
			log.Info("Pod: (RETURNING cluster-name=timescaledb) " + fmt.Sprintf("annotations: %v %s/%s", pod.Annotations, pod.Namespace, pod.Name))
			return admission.Allowed("Pod allowed")
		}
	}

	for i := range pod.Spec.Containers {

		if pod.Spec.Containers[i].Env == nil {
			pod.Spec.Containers[i].Env = []corev1.EnvVar{}
		}
		for j := range pod.Spec.Containers[i].Env {
			if pod.Spec.Containers[i].Env[j].Name == "TANGO_HOST" {
				hasTango = true
			}
		}

		// if this is a tango container add /home/tango to the PATH
		if hasTango {
			pod.Annotations["fix-permissions-mutating-admission-webhook-tango"] = "found TANGO_HOST"
			pod.Spec.Containers[i].Env = append(pod.Spec.Containers[i].Env, corev1.EnvVar{Name: "PATH", Value: "/home/tango:/env/bin:/home/tango/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"})
		}

		if pod.Spec.Containers[i].Lifecycle == nil {
			pod.Spec.Containers[i].Lifecycle = &corev1.Lifecycle{PostStart: &corev1.LifecycleHandler{Exec: &corev1.ExecAction{}}}
		}
		if pod.Spec.Containers[i].Lifecycle.PostStart == nil {
			pod.Spec.Containers[i].Lifecycle.PostStart = &corev1.LifecycleHandler{}
		}
		if pod.Spec.Containers[i].Lifecycle.PostStart.Exec == nil {
			pod.Spec.Containers[i].Lifecycle.PostStart.Exec = &corev1.ExecAction{}
		}
		// if this is tango then stick chown in HOME
		if hasTango {
			pod.Spec.Containers[i].Lifecycle.PostStart.Exec.Command = append(pod.Spec.Containers[i].Lifecycle.PostStart.Exec.Command, "/bin/sh", "-c", "cp -f /usr/bin/true /home/tango/chown; exit 0")
		} else {
			pod.Spec.Containers[i].Lifecycle.PostStart.Exec.Command = append(pod.Spec.Containers[i].Lifecycle.PostStart.Exec.Command, "/bin/sh", "-c", "cp -f /usr/bin/true /usr/bin/chown; exit 0")
		}
	}

	// if no tango then stick chown in /usr/bin and set user to root
	if !hasTango {
		pod.Annotations["fix-permissions-mutating-admission-webhook"] = "set securityContext to root"
		var nroot int64 = 0
		pod.Spec.SecurityContext.RunAsUser = &nroot
		pod.Spec.SecurityContext.RunAsGroup = &nroot
		pod.Spec.SecurityContext.RunAsNonRoot = nil
	}

	log.Info("Outgoing Pod: spec: " + fmt.Sprintf("%v", pod.Spec))

	log.Info("Pod: (PATCHED: set securityContext) " + fmt.Sprintf("operation: %s %s/%s", req.Operation, pod.Namespace, pod.Name))

	marshaledPod, err := json.Marshal(pod)
	if err != nil {
		return admission.Errored(http.StatusInternalServerError, err)
	}

	// admission.Response
	return admission.PatchResponseFromRaw(req.Object.Raw, marshaledPod)
}

func (a *PodAnnotator) InjectDecoder(d *admission.Decoder) error {
	a.decoder = d
	return nil
}
